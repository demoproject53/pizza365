"use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  // Biến lưu trữ địa chỉ API
  const gURL_DRINK_LIST = "http://localhost:8080/api/drinks";
  const gURL_VOUCHER = "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/";
  const gURL_ORDER = "http://203.171.20.210:8080/devcamp-pizza365/orders";
  // Biến lưu thông tin đơn hàng
  var gOrderRequest = {
          kichCo: "",
          duongKinh: "",
          suon: "",
          salad: "",
          loaiPizza: "",
          idVourcher: "",
          idLoaiNuocUong: "",
          soLuongNuoc: "",
          hoTen: "",
          phanTramGiamGia:0,
          thanhTien: 0,
          giamGia:0,
          tongTien: -1,
          email: "",
          soDienThoai: "",
          diaChi: "",
          loiNhan: ""
      }
  // Biến lưu combo pizza
  var gSelectedMenu = {
      kichCo:"",
      duongKinh:"",
      suon:"",
      salad:"",
      soLuongNuoc:"",
      thanhTien:""
  }
  // Biến lưu trữ loại pizza
  var gSelectedPizzaType = {
    loaiPizza:"",
  }
  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  getDrinkList();
  // Gắn sự kiện các nút chọn menu
  $('#btn-menu-small').on('click',function(){
    selectMenuStructure("small",20,2,200,2,150000);
  });
  $('#btn-menu-medium').on('click',function(){
    selectMenuStructure("medium",25,4,300,3,200000);
  });
  $('#btn-menu-large').on('click',function(){
    selectMenuStructure("large",30,8,500,4,250000);
  })

  // Gắn sự kiện các nút chọn loại pizza
  $('#btn-seafood-pizza').on('click',function(){
    selectPizzaType("seafood");
  });
  $('#btn-hawaii-pizza').on('click',function(){
    selectPizzaType("hawaii");
  });
  $('#btn-bacon-pizza').on('click',function(){
    selectPizzaType("bacon");
  });

  // Gắn sự kiện cho nút gửi đơn
  $('#btn-send-order').on('click',function(){
    onBtnSendOrderClick();
  })

  // Gán sự kiện cho nút tạo đơn
  $('#modal-create-order').on('click',function(){
    onBtnCreateOrder(gOrderRequest);
  })

  onPageLoading();


  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  
  function onPageLoading(){
    // Gọi APi để lấy thông tin khuyến mại
    $.ajax({
      url:"http://localhost:8080/api/campain",
      type:"GET",
      success: function(paramRes){
        $('#dicount-message').html(paramRes);
      },
      error: function(){
        alert('Fail!')
      }

    })
  }
  
  
  
  // Hàm xử lý chọn menu pizza
  function selectMenuStructure(kichCo, duongKinh , suon , salad , soLuongNuoc , thanhTien){
    gSelectedMenu.kichCo = kichCo;
    gSelectedMenu.duongKinh = duongKinh;
    gSelectedMenu.suon = suon;
    gSelectedMenu.salad = salad;
    gSelectedMenu.soLuongNuoc = soLuongNuoc;
    gSelectedMenu.thanhTien = thanhTien;
    console.log('Menu đã chọn:');
    console.log(gSelectedMenu);
    changeBtnChooseMenu(kichCo);
  }
  // Hàm xử lý chọn loại pizza
  function selectPizzaType(paramType){
    gSelectedPizzaType.loaiPizza= paramType;
    console.log('Loại pizza đã chọn: '+gSelectedPizzaType.loaiPizza);
    changeBtnChoosePizza(paramType)
  }

  // Hàm xử lý gửi đơn
  function onBtnSendOrderClick(){
    // B1: Thu thập thông tin đơn hàng
    getDataOrder(gOrderRequest);
    //B2: Kiểm tra thông tin
    var vCheck = validateData(gOrderRequest);
    //B3: Hiển thị lên modal
    if(vCheck == true){
      displayOrderModal(gOrderRequest);
    }
  }

  // Hàm xử lý tạo order
  function onBtnCreateOrder(paramOrder){
    $.ajax({
      url: gURL_ORDER,
      type:"POST",
      contentType: "application/json",
      data: JSON.stringify(paramOrder),
      success: function(paramRes){
        console.log(paramRes);
        createOrderSuccess(paramRes);
        $('#success-order-modal').modal('show');
      },
      error: function(){
        console.log('Fail')
      }
    })
  }


  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
  // Hàm đổi màu btn chọn menu pizza
  function changeBtnChooseMenu(paramSize){
    if(paramSize == "small"){
      $('#btn-menu-small').addClass('bg-success');
      $('#btn-menu-medium').removeClass('bg-success');
      $('#btn-menu-large').removeClass('bg-success');
    }
    else if (paramSize == "medium"){
      $('#btn-menu-small').removeClass('bg-success');
      $('#btn-menu-medium').addClass('bg-success');
      $('#btn-menu-large').removeClass('bg-success');
    }
    else if (paramSize == "large"){
      $('#btn-menu-small').removeClass('bg-success');
      $('#btn-menu-medium').removeClass('bg-success');
      $('#btn-menu-large').addClass('bg-success');
    }
  }

  // Hàm đổi màu nút chọn menu
  function changeBtnChoosePizza(paramPizza){
    if(paramPizza == "seafood"){
      $('#btn-seafood-pizza').addClass('bg-success');
      $('#btn-hawaii-pizza').removeClass('bg-success');
      $('#btn-bacon-pizza').removeClass('bg-success');
    }
    else if(paramPizza == "hawaii"){
      $('#btn-seafood-pizza').removeClass('bg-success');
      $('#btn-hawaii-pizza').addClass('bg-success');
      $('#btn-bacon-pizza').removeClass('bg-success');
    }
    else if(paramPizza == "bacon"){
      $('#btn-seafood-pizza').removeClass('bg-success');
      $('#btn-hawaii-pizza').removeClass('bg-success');
      $('#btn-bacon-pizza').addClass('bg-success');
    }
  }

  // Hàm gọi API lấy Drink List
  function getDrinkList(){
    $.ajax({
        url:gURL_DRINK_LIST,
        type:"GET",
        success: function(paramRes){
            //console.log(paramRes);
            loadDataToSelectDrink(paramRes);
        },
        error: function(){
            console.log('Fail!');
        }
    })
  } 
  // Hàm gọi API lấy mã giảm giá
  function getVoucher(paramVoucheID){
    $.ajax({
      url:gURL_VOUCHER + paramVoucheID,
      type:"GET",
      assync:false,
      success: function(paramRes){
        gOrderRequest.phanTramGiamGia = parseFloat(paramRes.phanTramGiamGia);
        gOrderRequest.tongTien = gOrderRequest.thanhTien*(1-gOrderRequest.phanTramGiamGia/100);
        gOrderRequest.giamGia = gOrderRequest.thanhTien - gOrderRequest.tongTien;
      },
      error: function(){
        console.log('Không có mã giảm giá này');
        gOrderRequest.phanTramGiamGia = 0;
        gOrderRequest.tongTien = gOrderRequest.thanhTien;
      }
    })
  }


  // Hàm load các loại nước uống vào select
  function loadDataToSelectDrink(paramObj){
    var vSelectDrink = $('#select-drink');
    for( var bI =0; bI <paramObj.length; bI ++){
      $('<option>',{text:paramObj[bI].tenNuocUong, value:paramObj[bI].maNuocUong}).appendTo(vSelectDrink);
    }
  }
  // hàm thu thập thông tin đơn hàng
  function getDataOrder(paramObj){
    paramObj.kichCo = gSelectedMenu.kichCo;
    paramObj.duongKinh = gSelectedMenu.duongKinh;
    paramObj.suon = gSelectedMenu.suon;
    paramObj.salad = gSelectedMenu.salad;
    paramObj.soLuongNuoc = gSelectedMenu.soLuongNuoc;
    paramObj.thanhTien = gSelectedMenu.thanhTien;
    paramObj.loaiPizza = gSelectedPizzaType.loaiPizza;
    paramObj.idLoaiNuocUong = $('#select-drink option:selected').text();
    paramObj.hoTen = $('#inp-ten').val().trim();
    paramObj.email = $('#inp-email').val().trim();
    paramObj.soDienThoai = $('#inp-dien-thoai').val().trim();
    paramObj.diaChi = $('#inp-dia-chi').val().trim();
    paramObj.idVourcher =$('#inp-giam-gia').val().trim();
    paramObj.loiNhan = $('#inp-loi-nhan').val().trim();
    getVoucher(paramObj.idVourcher);
  }
  
  // Hàm kiểm tra thông tin đơn hàng
  function validateData(paramOrder){
    if(paramOrder.kichCo == ""){
      alert('Hãy chọn combo pizza');
      return false;
    }
    if(paramOrder.loaiPizza == ""){
      alert('Hãy chọn loại pizza');
      return false;
    }
    if(paramOrder.idLoaiNuocUong == "Tất cả loại nước uống"){
      alert('Hãy chọn nước uống');
      return false;
    }
    if(paramOrder.hoTen == ""){
      alert('Hãy điền họ và tên');
      return false;
    }
    if(validateEmail(paramOrder.email) == false){  // validate email
      alert('Email không hợp lệ');
      return false;
    }
    if(paramOrder.soDienThoai == ""){
      alert('Hãy nhập số điện thoại');
      return false;
    }
    if(paramOrder.diaChi == ""){
      alert('Hãy nhập địa chỉ');
      return false;
    }
    return true;
  }
  // Hiển thị thông tin đơn hàng modal
  function displayOrderModal(paramOrder){
    $('#order-modal').modal('show');
    $('#inp-modal-ho-ten').val(paramOrder.hoTen);
    $('#inp-modal-dien-thoai').val(paramOrder.soDienThoai);
    $('#inp-modal-dia-chi').val(paramOrder.diaChi);
    $('#inp-modal-loi-nhan').val(paramOrder.loiNhan);
    $('#inp-modal-ma-giam-gia').val(paramOrder.idVourcher);
    
    $('#text-modal-info').val(`    Xác nhận: ${paramOrder.hoTen} - ${paramOrder.soDienThoai} - ${paramOrder.diaChi}
    Menu: ${paramOrder.kichCo}, sường nướng: ${paramOrder.suon}, ${paramOrder.idLoaiNuocUong}: ${paramOrder.soLuongNuoc}
    Loại pizza: ${paramOrder.loaiPizza}, Giá: ${paramOrder.thanhTien}, Mã giảm giá: ${paramOrder.idVourcher}
    Phải thanh toán: ${paramOrder.tongTien} (giảm giá ${paramOrder.phanTramGiamGia}%)
    Lời nhắn: ${paramOrder.loiNhan}`);
  }

  // Xử lý hiển thị mã đơn hàng ở modal thông báo
  function createOrderSuccess(paramObj){
    $('#order-modal').modal('hide');
    $('#success-order-modal').modal('show');
    $('#ma-don-hang').val(paramObj.orderCode);
  }

  // Hàm kiểm tra định dạng email
  function validateEmail(paramEmail) {
    var validRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (paramEmail.match(validRegex)) {
    return true;
    } 
    else {
    return false;
    }
  }



