package com.devcamp.pizza365v20.model;

public class Combomenu {
    private String menu;
    private int duongKinh;
    private int suonNuong;
    private int salad;
    private int nuocNgot;
    private int giaTien;

    public Combomenu(String menu, int duongKinh, int suonNuong, int salad, int nuocNgot, int giaTien) {
        this.menu = menu;
        this.duongKinh = duongKinh;
        this.suonNuong = suonNuong;
        this.salad = salad;
        this.nuocNgot = nuocNgot;
        this.giaTien = giaTien;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public int getDuongKinh() {
        return duongKinh;
    }

    public void setDuongKinh(int duongKinh) {
        this.duongKinh = duongKinh;
    }

    public int getSuonNuong() {
        return suonNuong;
    }

    public void setSuonNuong(int suonNuong) {
        this.suonNuong = suonNuong;
    }

    public int getSalad() {
        return salad;
    }

    public void setSalad(int salad) {
        this.salad = salad;
    }

    public int getNuocNgot() {
        return nuocNgot;
    }

    public void setNuocNgot(int nuocNgot) {
        this.nuocNgot = nuocNgot;
    }

    public int getGiaTien() {
        return giaTien;
    }

    public void setGiaTien(int giaTien) {
        this.giaTien = giaTien;
    }
}
