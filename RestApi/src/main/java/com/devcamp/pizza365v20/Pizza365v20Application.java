package com.devcamp.pizza365v20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pizza365v20Application {

	public static void main(String[] args) {
		SpringApplication.run(Pizza365v20Application.class, args);
	}

}
