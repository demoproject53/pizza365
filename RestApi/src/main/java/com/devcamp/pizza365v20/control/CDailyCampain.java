package com.devcamp.pizza365v20.control;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365v20.model.Combomenu;
import com.devcamp.pizza365v20.model.Drink;

@RestController
@CrossOrigin
public class CDailyCampain {
    @GetMapping("/campain")
    public String getDiscount(){
        LocalDateTime date = LocalDateTime.now();
        DayOfWeek dayWeak = DayOfWeek.from(date);
        int dayWeakNum = dayWeak.getValue();
        switch(dayWeakNum){
            case 0:
                return "Thứ 2 mua hai tặng một";
            case 1:
                return "Thứ 3 mua một tính tiền ba";
            case 2:
                return "Thứ 3 không có quà";
            case 3:
                return "Thứ 4 tặng một coca";
            case 4:
                return "Thứ 5 freeship đơn dưới 100k";
            case 5:
                return "Thứ 6 giảm giá 16%";
            default:
                return "Không có khuyễn mãi";
        }
    }

    @GetMapping("/combo-menu")
    public List<Combomenu> getComboMenu(){
        Combomenu small = new Combomenu("small", 20, 2, 200, 2, 150000);
        Combomenu medium = new Combomenu("medium", 25, 4, 300, 3, 200000);
        Combomenu large = new Combomenu("large", 30, 8, 500, 4, 250000);
    
        List<Combomenu> menuList = new ArrayList<>();
        menuList.add(small);
        menuList.add(medium);
        menuList.add(large);
        return menuList;
    }

    @GetMapping("/drinks")
    public List<Drink> getDrinks(){
        
        Drink drink1 = new Drink("TRATAC", "Trà tắc", 10000);
        Drink drink2 = new Drink("COCA", "Cocacola", 15000);
        Drink drink3 = new Drink("PEPSI", "Pepsi", 15000);
        Drink drink4 = new Drink("LAVIE", "Lavie", 5000);
        Drink drink5 = new Drink("TRASUA", "Trà sữa trân châu", 40000);
        Drink drink6 = new Drink("FANTA", "Fanta", 15000);

        List<Drink> List = new ArrayList<>();
        List.add(drink1);
        List.add(drink2);
        List.add(drink3);
        List.add(drink4);
        List.add(drink5);
        List.add(drink6);
     
        return List;
    }

}
